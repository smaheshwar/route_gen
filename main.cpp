#include "date/tz.h"
#include "route.h"

void 
test_time_arithmetic()
{
	time_point tp = 
          std::chrono::system_clock::from_time_t(std::time(nullptr));
	std::cout << "tp = " << tp << std::endl;
	duration till_date_change = mins_till_midnight(tp);
	std::cout << "till_date_change = " << till_date_change << "\n";
	duration dur(5);
	time_point tp2 = tp + dur;
	std::cout << "after adding tp2 = " << tp2 << std::endl;
	duration diff = std::chrono::duration_cast<std::chrono::minutes>(tp - tp2);
	std::cout << "diff = " << diff << "\n";
	duration dur2(10);
	tp -= dur2;
	std::cout << "after subtracting tp = " << tp << std::endl;
	std::string tmstr("2020-03-31 09:31");
	time_point parsed = parse_time_point(tmstr);
	std::cout << "parsed = " << parsed << "\n";
}

void test_simulate_driving()
{
	using std::cout;
	using std::endl;
	std::string tmstr("2020-03-31 09:00");
	time_point start_time = parse_time_point(tmstr);
	double dstn_dist = 250.0; // miles
	double speed = 50.0; // mph
	node start_node;
	start_node.timepoint = start_time;
	start_node.hos.hos_8 = duration(30);
	start_node.hos.hos_11 = duration(11*60);
	start_node.hos.hos_14 = duration(14*60);
	start_node.hos.hos_70 = duration(70*60);
	start_node.hos.duty_since_last_midnight = duration(0);
	for(size_t i = 0; i < start_node.hos.recaps.size(); ++i) {
		start_node.hos.recaps[i] = duration(0);
	}
	cout << "start_node" << endl;
	cout << start_node << endl;
	cout << "dstn_dist = " << dstn_dist << endl;
	cout << "speed = " << speed << endl;
	vec1d<segment> segs = 
	simulate_driving(start_node, dstn_dist, speed, segment_type::dhd_travel);
	cout << "segs from simulate_driving" << endl;
	cout << segs << endl;
	cout << "Cumulative distance covered" << endl;
	for(const auto s : segs) {
		if(s.type == segment_type::dhd_travel || 
			s.type == segment_type::rev_travel) {
			using std::chrono::duration_cast;
			duration dur = duration_cast<std::chrono::minutes>(s.end.timepoint - 
															s.begin.timepoint);
			cout << dur.count()*speed/60 << endl;
		}
	}
}

void test_get_next_midnight()
{
	std::cout << "test_get_next_midnight()" << std::endl;
	std::string tmstr("2020-03-31 09:31");
	time_point tp = parse_time_point(tmstr);
	std::cout << "parsed tp = " << tp << "\n";
	std::cout << "Calling get_next_midnight(tp)" << std::endl;	
	std::cout << get_next_midnight(tp) << std::endl;	
	std::cout << "done" << std::endl;
}

void test_is_midnight()
{
	using std::cout;
	using std::endl;

	cout << "test_is_midnight()\n";
	
	std::string tmstr1("2020-03-31 09:31");
	time_point tp1 = parse_time_point(tmstr1);
	bool result1 = is_midnight(tp1);
	cout << "tp1 = " << tp1 << endl;
	cout << "result1 = " << result1 << endl;
	
	std::string tmstr2("2020-04-01 00:00");
	time_point tp2 = parse_time_point(tmstr2);
	bool result2 = is_midnight(tp2);
	cout << "tp2 = " << tp2 << endl;
	cout << "result2 = " << result2 << endl;
	
	std::string tmstr3("2020-04-01 00:01");
	time_point tp3 = parse_time_point(tmstr3);
	bool result3 = is_midnight(tp3);
	cout << "tp3 = " << tp3 << endl;
	cout << "result3 = " << result3 << endl;
}

void test_tz()
{
	std::cout << "test_tz()\n";
	using namespace date;
    using namespace std::chrono;
    auto t = make_zoned(current_zone(), system_clock::now());
    std::cout << t << '\n';
	std::cout << "done\n";
}


void test_tz_conversion()
{
	using namespace date;
    using namespace std::chrono;
    auto utc = system_clock::now();
    auto berlin = make_zoned("Europe/Berlin", utc);
    auto local  = make_zoned(current_zone(), berlin);
    auto utc2   = berlin.get_sys_time();
    std::cout << format("%F %T %Z", utc) << '\n';
    std::cout << format("%F %T %Z", berlin) << '\n';
    std::cout << format("%F %T %Z", local) << '\n';
    std::cout << format("%F %T %Z", utc2) << '\n';
}


void test_update_recaps()
{
	std::array<duration, 7> recaps = {
										duration(0), 
										duration(1), 
										duration(2), 
										duration(3), 
										duration(4), 
										duration(5), 
										duration(6)
									};
	duration accum_duty(7);
	std::cout << "accum_duty = " << accum_duty << "\n";
	std::cout << "recaps before:\n";
	std::cout << recaps << "\n";
	update_recaps(recaps, accum_duty);
	std::cout << "recaps after:\n";
	std::cout << recaps << "\n";
}

void test_deplete_clocks()
{
	// Test cases for function deplete_clocks()
	// 1. depl_14hr_only=false, make sure clocks don't become negative
	// 2. depl_14hr_only=true, make sure clocks don't become negative
}

void test_reset_clock()
{
	// Test cases for function reset_clock()
	// Create arbitrary values for hos (make sure none are full)
	// Call function with different values of total_off_duty_dur
	// 1. total_off_duty_dur >= 34hrs
	// 2. 34 hrs > total_off_duty_dur >= 10hr
	// 3. 10hr > total_off_duty_dur >= 30min
	// 4. total_off_duty_dur < 30min
	// Make sure appropriate clocks are reset.
}

void test_get_next_node_off_duty()
{
	// Test cases for function get_next_node_off_duty()
	// Create prev_node with a certain time
	// Call function with arbitrary values of cur_time
	// 1. cur_time represents midnight, midnight flag = true
	// 2. cur_time does not represent midnight flag = false (assertion fail)
	// Only 14 hour clock should deplete, should not become negative
}


int main(int argc, char **argv) {
	//test_get_next_midnight();
	//test_tz();
	//test_tz_conversion();
	// test_update_recaps();
	// test_is_midnight();
	test_simulate_driving();
	return 0;
}
