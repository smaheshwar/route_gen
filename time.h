#pragma once

#include <chrono>
#include <ctime>
#include <iostream>
#include <cassert>
#include <cmath>

using time_point = std::chrono::time_point<std::chrono::system_clock>;
using duration = std::chrono::minutes;

#define CHECKPOINT(s) std::cout<<__FILE__<<":"<<__LINE__<<": "<<s<<std::endl


// allows equality comparison in one line 
// "if(x == y == z)" can be written as "if(all_equal(x, y, z))"
template<typename T, typename U>
bool all_equal(T&& t, U&& u)
{
	return (t == u);
}
template<typename T, typename U, typename... Ts>
bool all_equal(T&& t, U&& u, Ts&&... args)
{
	return (t == u) && all_equal(u, std::forward<Ts>(args)...);
}

bool
is_midnight(const time_point& tp);

time_point
get_last_midnight(const time_point& tp);

time_point
get_next_midnight(const time_point& tp);


duration
mins_since_midnight(const time_point &tp);


duration
mins_till_midnight(const time_point &tp);


time_point
parse_time_point(const std::string &s);


std::ostream&
operator<<(
	std::ostream& os,
    const time_point &tp
	);

std::ostream&
operator<<(
	std::ostream &os,
	const duration &dur
	); 


