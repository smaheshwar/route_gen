#pragma once

#include <vector>
#include <array>
#include <list>
#include <forward_list>
#include <algorithm>
#include "time.h"

template <class T>
using vec1d = std::vector<T>;

template <class T>
using vec2d = std::vector<std::vector<T>>;

template <class T>
using refwrap = std::reference_wrapper<T>;

template <class T>
using flist = std::forward_list<T>;

// Encapsulates coordinates of a location.
// May get rid of this later and retain the floating point representation
// used by HERE and USX IT database.
struct deg_min_sec {
	int deg;
	int min;
	int sec;
};

// Represents latitude-longitude of a location.
struct lat_long {
	deg_min_sec lat;
	deg_min_sec lng;
	lat_long(
		const deg_min_sec &new_lat,
		const deg_min_sec &new_lng
	):
		lat(new_lat),
		lng(new_lng)
	{}
};


// Represents customer location. 
// Customer locatin can be either pickup or dropoff point.
// Dropoff points have a specific ordering.
// dist_from_prev represents distance from previous dropoff point. 
// Will be 0 for pickup site, distance from previous location to this location 
// for dropoff sites.
struct customer_location {
	lat_long location;
	time_point window_start; 
	time_point window_end;
	duration load_unload; // load time for pickup site, unload time for dropoff site
	bool require_trailer;
	bool trailer_available_for_pickup;
	double dist_from_prev; // 0 for pickup site, distance from previous location to this location for dropoff sites
	// Time zone (to allow conversion to truck's home time zone)
};


// Represents customer order which includes a pickup location and at least
// one dropoff location. Dropoff locations are in a specific order.
struct customer_order {
	int order_number;
	double revenue;
	customer_location pickup;
	vec1d<customer_location> dropoff;
};


// Represents a truck. A truck includes a tractor and driver.
// It is assumed a driver and a tractor stay together for the entire route.
struct truck {
	int driver;
	int tractor;
	time_point pta;
	lat_long pta_loc;
	time_point ht; // must be home by this time, hard limit for now 
	lat_long ht_loc; 
	vec1d<customer_order> frozen_orders; // truck cannot pickup new orders until these orders are completed
	// HT timezone 
};


// Represents HOS clocks. All values are in duration (minutes resolution).
// recaps array stores recap values. recaps[0] is the oldest recap value.
// duty_since_last_midnight is the total accumulated duty since prior midnight.
struct hos_clocks {
	duration hos_8; // 8 hr clock remaining
	duration hos_11; // 11 hr clock remaining
	duration hos_14; // 14 hr clock remaining
	duration hos_70; // 70 hr clock remaining
	std::array<duration, 7> recaps; 
	duration duty_since_last_midnight;
	hos_clocks() {}
	hos_clocks(const hos_clocks &h):
		hos_8(h.hos_8),
		hos_11(h.hos_11),
		hos_14(h.hos_14),
		hos_70(h.hos_70),
		recaps(h.recaps),
		duty_since_last_midnight(h.duty_since_last_midnight) {}
};


// Represents a point on the timeline as vehicle travels from origin to 
// destination. This point represents a change of state of the vehicle. 
// Stores the time when the state change occurs and the associated hos_clock
// object at that point.
struct node {
	time_point timepoint;
	hos_clocks hos;
	node() {}
	node(const node &n):
		timepoint(n.timepoint),
		hos(n.hos) {}
	node(
		const time_point &t,
		const hos_clocks &h
	) :
		timepoint(t),
		hos(h)
	{}
};


// Represents the type of the segment on the time line.
// This is akin to the state of the vehicle.
// dhd_travel and rev_travel imply the vehicle is moving. 
// All others imply vehicle is stationary. 
enum class segment_type {
	dhd_travel,
	rev_travel,
	load,
	unload,
	dwell,
	rest_30m,
	rest_10h,
	rest_34h,
	//personal_travel,
	//home
};


// Represents a segment on the timeline. A segment consists of a begin node
// and end node. This shows the state the vehicle is in between the two nodes.
// A sequence of segments represents the vehicle travel from an origin to 
// destination.
struct segment {
	node begin;
	node end;
	segment_type type;
	segment(
		const node &b,
		const node& e,
		segment_type st
	) : begin(b),
		end(e),
		type(st)
	{}
};


// Represents pickup and delivery for a given order.
struct order_delivery {
	const customer_order& order;
	vec1d<segment> segments;
	order_delivery(
		const customer_order &co
	) : order(co)
	{}
};

// Represents a route for a truck (rename to driver_journey?)
// deliveries - sequence of pickups and drop-offs for this truck
// travel_home - timeline for journey back home (dhd only), can be zero length
struct truck_route {
	const truck& tr;
	vec1d<order_delivery> deliveries;
	vec1d<segment> travel_home; 
	truck_route(
		const truck &t
	) : tr(t)
	{}
};


// Enumerates all the possible epochs or events that can occur as the truck
// moves from origin to destination. These events can be a midnight crossing,
// some HOS clock running out or reaching the destination.
struct driving_epochs
{
	const static int midnight = 0;
	const static int dstn_arrival = 1;
	const static int clk_8hr_runs_out = 2;
	const static int clk_11hr_runs_out = 3;
	const static int clk_14hr_runs_out = 4;
	const static int clk_70hr_runs_out = 5;
};


// Prints recaps array.
std::ostream&
operator<<(
	std::ostream &os,
	std::array<duration, 7> recaps
	);

// Prints hos object.
std::ostream&
operator<<(
	std::ostream &os,
	const hos_clocks &hos
	);

// Prints hos object.
std::ostream&
operator<<(
	std::ostream &os,
	const node &n
	);

// Prints segment_type.
std::ostream&
operator<<(
	std::ostream &os,
	const segment_type st
	);


// Prints the vector of segment objects.
std::ostream&
operator<<(
	std::ostream &os,
	const vec1d<segment> &segs
	) ;


// Main function for simulating a vehicle driving from an origin to destination.
// Calculates the nodes and segments as vehicle moves from one state to another.
// Parameters:
// start_node - Represents the origin of the timeline.
// dstn_dist - Distance to destination (assme miles)
// speed - Average speed (assume miles per hour)
// st - Type of the segment when the vehicle is moving. The caller needs to 
// be aware whether vehicle is deadheading (moving towards a pickup location or
// traveling home), or carrying load (moving towards a drop-off location).
// Returns a vector of segment objects representing the travel of the vehicle.
vec1d<segment>
simulate_driving(
	const node& start_node,
	double dstn_dist,
	double speed,
	segment_type st // should be either dhd or rev travel
);


void
deplete_clocks(
	hos_clocks& hos,
	const time_point& t1,
	const time_point& t2,
	bool depl_14hr_only
	);

node
get_next_node_off_duty(
	const node& prev,
	const time_point& cur_time,
	bool is_midnight
	);

node
get_next_node_on_duty(
	const node& prev,
	const time_point& cur_time,
	bool is_midnight
	);

void 
reset_clocks(
	hos_clocks& hos, 
	const duration& total_off_duty_dur
	);

vec1d<node>
simulate_stationary_off_duty(
	const node& start_node,
	const time_point& end_tm
	);

vec1d<node>
simulate_stationary_on_duty(
	const node& start_node,
	const time_point& end_tm
	);

time_point
get_dstn_arrival(
	const time_point& start_time,
	const time_point& cur_time,
	double dstn_dist,
	double speed 
	);

node
get_next_node_driving_midnight(
	const node& prev_node,
	const time_point& cur_time // assume midnight
	);


node get_next_node_driving_arrive_dstn(
	const node& prev_node,
	const time_point& cur_time 
	);

vec1d<segment>
get_rest_segments(
	const node& rest_start_node,
	const duration &rest_dur,
	const segment_type st 
	);

std::array<time_point, 6>
get_driving_epochs(
	const time_point &t,
	const hos_clocks &hos,
	const time_point &dstn_arrival
	);

segment
get_next_segment_driving_midnight(
	const node& prev_node,
	const time_point& t,
	const segment_type st
	);

segment
get_next_segment_driving_dstn_arrival(
	const node& prev_node,
	const time_point& t,
	const segment_type st
);

vec1d<segment>
get_rest_segments_driving(
	const node& prev_node,
	const time_point& t,
	segment_type st,
	const duration &rest_dur
	);

vec1d<segment>
process_next_epoch_driving(
	int indx,
	const time_point& t,
	const node &prev_node,
	segment_type st
	);


void
update_recaps(
	std::array<duration, 7> &recaps,
	const duration &accumulated_duty
	);

double
get_dist_covered(
	const time_point &t1,
	const time_point &t2,
	double speed
);

double
get_total_dist_covered(
	const vec1d<segment> segs,
	double speed
);

vec1d<segment>
simulate_cust_site_visit(
	const node &start_node,
	double dstn_dist,
	const time_point &earliest_arrival,
	const time_point &latest_arrival,
	bool is_pickup,
	const duration& load_unload, 
	double speed
);

vec1d<segment>
simulate_dwell(
	const node &dwell_start_node,
	const time_point &dwell_end
);

vec1d<segment>
simulate_load_unload(
	const node &start_node,
	const time_point &end_tm,
	segment_type st
);