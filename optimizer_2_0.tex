\documentclass[12pt]{article}

\usepackage{xcolor}
\definecolor{commentgreen}{RGB}{2,112,10}
\definecolor{eminence}{RGB}{108,48,130}
\definecolor{weborange}{RGB}{255,165,0}
\definecolor{frenchplum}{RGB}{129,20,83}

\usepackage{amsmath}
\usepackage{listings}
\usepackage{textcomp}
\lstset {
	language=C++,
	frame=tb,
	tabsize=4,
	showstringspaces=false,
	numbers=left,
	upquote=true,
	commentstyle=\color{commentgreen},
	keywordstyle=\color{eminence},
	stringstyle=\color{red},
	basicstyle=\small\ttfamily, % basic font setting
	emph={int,char,double,float,unsigned,void,bool},
	emphstyle={\color{blue}},
	escapechar=\&,
	% keyword highlighting
	classoffset=1, % starting new class
	morekeywords={>,<,.,;,,,-,!,=,~},
	keywordstyle=\color{weborange},
	classoffset=0,
}


%opening
\title{Truck Route Optimizer}

\author{Siddhartha Maheshwary}

\begin{document}

\maketitle

\begin{abstract}
This document describes strategies for optimizing truck routes.
The problem of truck routing can be described as follows.
We are given a set of trucks, and a set of customer orders that need to be 
picked up at specified locations, and dropped-off at specified locations. 
A given customer order may have  multiple drop-off points that must be visited 
in specified sequence. Both pickup and drop-off locations have time-windows 
associated with them. 
Thus, this problem is very similar to the classic vehicle-routing problem 
with pickup and drop-off time windows. 
\end{abstract}


\part{Path-Based Strategy}

The path-based optimization strategy consists of two main steps. 
\begin{enumerate}
	\item Generate routes for given set of trucks and orders.
	\item Select the optimal sub-set of routes from the above set.
\end{enumerate}

This type of optimizer has three basic components.

\begin{enumerate}
	\item Route validation.
	\item Route generation.
	\item Route selection.
\end{enumerate}

\section{Route Validation}
This component determines the legality of a route. 
It needs to keep track of hours-of-service clock amounts,
recaps, duty accumulated since prior midnight, rest calculations, 
as well as any other logic needed to validate a route. 

The route validation component simulates the travel of the truck from origin
to destination to validate the routes. The simulation views the truck moving 
along a timeline, transitioning from one state to another. 

\subsection{Hours Of Service Rules}
The route validation is performed according to the ``hours of service'' (HOS) 
rules. According these rules, a 30 minute rest is trigerred when the 8-hour 
runs out. A 10 hour rest is trigerred when the 11- or 14-hour clock runs out.
A 34 hour rest is trigerred when the 70-hour clock runs out. 
It is assumed that the driver takes the full rest once it begins. 

8-hour, 11-hour and 14-hour clocks are checked continuously. But 70-hour clock
is checked at midnight only. Therefore, it is important to keep track of 
midnight crossings. The implementation regards midnight crossing as a possible
event node. 

The 70-hour clock calculation is based on the recap array (seven elements) 
and an additional variable that keeps track of duty accumulated since 
last midnight. At every midnight, the first element of recap array is added to 
the current value of the 70-hour clock, and the duty accumulated since midnight
is subtracted from the 70-hour clock. The clock value is not allowed to become
negative. After updating the clock, the array elements are shifted leftwards
so that the first element is droped, and the duty accumulated since last 
midnight is inserted as the last element of the array.

The 14-hour clock is assumed to deplete continuously all the time, 
even during rest. It is not allowed to go negative.

\subsection{Travel Simulation}

The travel simulation is implemented using the classic discrete-event simulation
model. A simulation clock is used that, at any given time, is advanced to the 
next possible event. Other data structures keep track of the current state 
of the truck.

\subsubsection{Simulation States And Events}

The truck can be in any one of the following states at any given time.

\begin{enumerate}
	\item Deadhead driving
	\item Revenue driving
	\item Dwelling
	\item Loading
	\item Unloading
	\item Resting
	\begin{enumerate}
		\item 30 minute rest
		\item 10 hour rest
		\item 34 hour rest
	\end{enumerate}
\end{enumerate}

The simulation considers the timeline to consist of one or more segments. 
The truck can be in any one of the above state during a given segment. 
A segment consists of two nodes, start and end. 
Thus, the nodes on the timeline represent transition of the truck from one 
state to another.

At any given time during the simulation, the following events can occur.

\begin{enumerate}
	\item Midnight crossing is encountered.
	\item Truck reaches the destination.
	\item 8-hour clock runs out.
	\item 11-hour or 14-hour clocks run out.
	\item 70-hour clock runs out.
\end{enumerate}

The simulation is divided into three broad cases.

\begin{enumerate}
	\item Vehicle is being driven (driving is always considered on-duty).
	\item Vehicle is stationary and off-duty (resting, dwelling).
	\item Vehicle is stationary but on-duty (loading, unloading).
\end{enumerate}

All clocks deplete when driver is on-duty. Only 14-hour clock depletes when
driver is off-duty. As stated above, no clocks are allowed to be negative,
and 70-hour clock is updated at midnights only. If a midnight crossing occurs
when driver is off-duty, then only recaps and duty acumulated since previous
midnight are updated.



\section{Route Generation}
This component enumerates the routes for a given set of trucks and orders. 
Since total number of possible routes is exponential in nature, 
it needs to do this enumeration in an intelligent manner, possibly employing
heuristics to prune and limit the search space. 
This component will rely on the route validation component to 
determine if a route is legal.

\subsection{Distance Matrix}
It is very important to have accurate driving distances for route generation. 
Currently we are relying on HERE API subscription service for distances. 
It will be too slow to query the HERE server repeatedly for different 
origin-destination pairs. Therefore we need to have a process that performs a
bulk download of HERE distances and caches these distances locally. 

\section{Route Selection}
This component selects the optimal set of routes. 
Each route is assigned a score that measures its goodness. 
The route selection process constructs the optimal subset such that total 
score is maximized. 
It also makes sure that the following constraints are satisfied.

\begin{enumerate}
	\item An order is covered by at most one route.
	\item A truck is assigned to at most one route.
\end{enumerate}

The problem of route selection is formulated as a pure binary integer program
(see below). This can be solved by a commercial solver like Gurobi or CPLEX.

\section{Formulation}
\subsection{Sets, Definitions and Assumptions}
\begin{itemize}
	\item Let $T$ be the set of trucks. 
	\item Let $L$ be the set of orders (used interchangeably with loads).
	\item Let $R$ be the set of routes.
\end{itemize}

A truck consists of a tractor and a driver. 
An order (or load) consists of one pick up location and one or more drop-off
locations. 
A route consists of a truck and a set of loads that this truck delivers.
It is assumed that the route generation component generates valid routes only.
It relies on the route validation components to make sure required rests, 
pickup and drop-off time windows, home time etc. are honored.

\section{Formulation}

\subsection{Decision Variables}
For any $j \in R$,

 \[
x_j = \left\{
\begin{array}{ll}
1 \text{ if route } j \text{ is selected}\\
0 \text{ otherwise}
\end{array}
\right.
\]



\subsection{Objective Function}

\subsubsection{Profit Calculation}
Let $c_j$ be the total cost of route $j \in R$. Let $r_j$ be the total 
revenue gained from route $j \in R$. 
The profit for route $j \in R$ is given by $p_j = r_j - c_j$. 
If $p_j \leq 0$ for any $j \in R$, then the optimizer may not select the route
$j \in R$ if constraints are modeled as less-than-or-equal. To get around this
problem, a constant term can be added to ensure all $p_j$'s are positive. 
Alternatively, the constraints for critical orders can be modeled as equality
constraints. But this approach requires that sufficient routes are enumerated 
to allow coverage of critical orders. otherwise the problem may be infeasible.
 

\subsubsection{Profit Maximization}
Goal is to maximize total profits, that is

\[
\text{maximize } \sum_{j \in R} p_j x_j
\]

\subsection{Constraints}

\subsubsection{One Truck Per Route}

For all $i \in T$, $j \in R$, define the parameter

 \[
a_{ij} = \left\{
\begin{array}{ll}
1 \text{ if truck } i \text{ services route } j\\
0 \text{ otherwise}
\end{array}
\right.
\]

Therefore,

\[
\sum_{j \in R} a_{ij} x_j \leq 1 \text{ } \forall \text{ } i \in T
\]

Note: Inequality allows some drivers to remain unassigned if enough work is not
available. Forcing equality might make it infeasible for some scenarios. 
The profit maximization objective function, however, will ensure utilization
of as many trucks as possible if the profits are positive.

\subsubsection{One Load Per Route}

For all $i \in L$, $j \in R$, define the parameter

 \[
b_{ij} = \left\{
\begin{array}{ll}
1 \text{ if load } i \text{ is serviced by route } j\\
0 \text{ otherwise}
\end{array}
\right.
\]


Therefore,

\[
\sum_{j \in R} b_{ij} x_j \leq 1 \text{ } \forall \text{ } i \in L
\]

Inequality allows some loads to remain un-serviced. 
Forcing equality might make it infeasible for some scenarios. 
The profit maximization objective function, however, will ensure utilization
of as many trucks as possible if the profits are positive.






\part{Flow Model}

In flow model we envision different entities (drivers, tractors, trailers, loads) 
flowing across a time-expanded network. Here the decision variable is whether or not
a particular entity flows along a certain edge (rather than whether a particular route
is selected or not). 

\section{Sets}
Let

\begin{itemize}
	\item $L$ be the set of all locations (customer sites, PTA locations, home
	 locations, terminals),
	\item $T$ be the set of all time points (discretize in 15 minute intervals?),
	\item $DRIVE$ be the set of all drivers,
	\item $TRAC$ be the set of all tractors,
	\item $TRAIL$ be the set of all trailers, and
	\item $LOAD$ be the set of all loads.
\end{itemize} 

\section{Time-Expanded Network}
Define directed acyclic graph $G=(V,E)$ with vertex set $V = L \times T$, 
and edge set $E$ is the set of ordered pairs $(i,j)$ such that 
it is possible to travel from $i \in V$ to $j \in V$.

If it can be proved that certain edges can never be part of the optimal solution, 
then they should not be created. 
For example, after being picked-up, a load should either go straight to its drop-off point,
or to a transfer location. It does not make sense for it to go anywhere else. Therefore, there is no
need to create other edges for any given load.

\section{Entities, Sources and Sinks}
Let $F = \{ DRIVE \cup TRAC \cup TRAIL \cup LOAD \}$ be the set of all entities 
that can flow in the time-expanded network $G$. 

For any $i \in F$, let 
$i_{source} \subset V$ be the set of source-nodes for this entity. 
For example, a load available for pickup at a customer site within a time window
will have a number of source vertices in the graph $G$. 


Similarly, for any $i \in F$, let $i_{sink} \subset V$ be the set of sink-nodes for 
this entity. For example, a load may have a number of sink-nodes base on the physical
drop-off location and time window.

\section{Lazy Constraints}
The biggest problem with flow model is that it becomes difficult
to incorporate many non-linear rules and constraints (e.g. max duty in a 70 hour window). 

For such rules we can add ``lazy constraints'' similar to sub-tour elimination constraints. Gurobi now
provides an easy way to add lazy constraints and internally manages them. It is hard
to imagine how such a large constraint matrix can converge in a reasonable time, but 
might be a good experiment. Could also run the flow model in parallel and let
both algorithms race to see which produces a better solution faster. Or just solve LP-relaxation for the
flow model, and use the solution to inform route generation in the path-based model.

\section{Formulation}
Note that this formulation is similar to a multi-commodity network flow.

\subsection{Decision Variables}

For any $i \in F$, $(j,k) \in E$, let

\[
x_{ijk} = 
	\left\{
		\begin{array}{ll}
			1 \text{ if $i$ flows along edge $(j,k)$ } \\
			0 \text{ otherwise}
		\end{array}
	\right.
\]


\subsection{Objective Function}
For any $i \in F$, $(j,k) \in E$, let $c_{ijk}$ be the cost of flowing along the 
edge $(j,k)$. For drivers, tractors and trailers it will be the usual costs 
(wages, wear and tear, fuel etc.). 
For loads, this cost will be negative (payoff) for any edge that comes into the
drop-off site within the drop-off time window, and zero for all other edges.

Thus the objective function is to minimize total cost for all entities flowing 
along the edges in the time-expanded network.

\[
	\text{minimize } \sum_{i \in F} \sum_{(i,j) \in E} c_{ijk} x_{ijk}
\]

\subsection{Constraints}

\subsubsection{Source Flow Balance}
An entity flows on at most one edge when starting from the source-node. 

\[
	\sum_{(j,k) \in E} x_{ijk} \leq 1 \text{ $\forall$ $i \in F$, $j \in i_{source}$}
\]

\subsubsection{Flow Balance At Non-Source-Sink}
Flow-balance for vertices that are not source or sink.

\[
	\sum_{(j,k) \in E} x_{ijk} = \sum_{(k,l) \in E} x_{ikl}
		\text{ $\forall$ 
			$i \in F, k \in V \setminus 
				\{i_{source} \cup i_{sink}\}$
			} 
\]

Flow balance at sink-nodes is not needed with the above constraints. We do not need
to enforce that a load be picked up, or force any entity to move. The objective 
should take care of that.

\subsubsection{Flow Dependencies}
Flow of any entity is dependent on another entity flowing. For example, loads cannot
move without trailer, trailer cannot move without tractor etc.

For any $i \in F$, let $i_{depend} \subset F$ such that

\[
	i_{depend} = 
		\left\{
			\begin{array}{ll}
				TRAIL \text{ if $i \in LOAD$,} \\
				TRAC \text{ if $i \in TRAIL$,} \\
				DRIVE \text{ if $i \in TRAC$}
			\end{array}
		\right.
\]




\[
	x_{ijk} \leq \sum_{l \in i_{depend}} x_{ljk} 
		\text{ $\forall$ $i \in F$, $(j,k) \in E$}
\]


Note that problem size can be reduced if the dependecies are more restrictive (e.g. only some trucks 
can service a load rather than any truck in the fleet).

\subsubsection{HOS Clocks}
Keeping track of HOS clock values is more difficult. It will require continuous variables to accumulate HOS
values at each node of the time-expanded network, and binary variables to register when this value becomes 
negative. If HOS value at any node is negative, then the $x_{ijk}$ must be restricted such that driver is
forced to stay at the current physical location. Another problem is that it cannot handle window rules 
(like the 70-hour clock). Hence any solution will be only approximately valid.

Let $h_{ij}$ (continuous variable) be the value of hos clock remaining for driver 
$i \in DRIVE$ at vertex $j \in V$.
Let $d_{jk}$ be the travel time for $(j,k) \in E$. 
Let $y_{ij} \in \{0,1\}$ be a binary variable that tracks when the clock runs out.

%For every $i \in V$, let $i_{move} \subset V$ be the set of vertices 
%such that for any $j \in i_{move}$, $(i,j) \in E$, 
%and $(i,j)$ represent distinct physical locations 
%(that is, movement along $(i,j)$ implies movement in physical world also).

for every $i \in V$, let $i_{rest} \subset V$ be the set of vertices such that
for any $j \in i_{rest}$, $(i,j) \in E$, and $(i,j)$ represents the driver being
stationary (resting) for the required duration of time.

\[
	h_{ij} - d_{jk} x_{ijk} = h_{ik}
		\text{ $\forall$ $i \in DRIVE$, $(j,k) \in E$ such that $k \notin j_{rest}$}
\]


\[
	h_{ij} + \epsilon \leq M y_{ij}
		\text{ $\forall$ $i \in DRIVE$}
\]

\[
	1 - y_{ij} \leq \sum_{(j,k) \in i_{rest}} x_{ijk}
\]

%\[
%	\sum_{(j,k) \in i_{move}} x_{ijk} \leq y_{ij}
%\]

\[
	M x_{ijk} \leq h_{ij} \text{ $\forall$ $(j,k) \in E$ such that $k \in j_{rest}$}
\]

Where $\epsilon$ is a given positive threshold (usually $1e-5$), 
and $M$ is the upper bound on the clock value.

(NOTE: The last inequality resets the clock if sufficient rest is given)

TODO: Do we need to put $y_{ij}$ in objective so optimization will try to minimize it? 
But the way it is linked with $x_{ijk}$, it might be automatically minimized since optimization is 
minimizing overall cost.


\section{Pros and Cons}

\subsection{Pros}
The flow model does not require route generation. 
Routes are created automatically by the decision variables turning on and off.

\subsection{Cons}
Many rules and constraints are difficult to model. For example, detailed rest calculation cannot be performed
in the flow model. Window rules cannot be included.
If any new rule changes happen in the future, then mathematical formulation
may need to be a modified. Formulations also tend to be very large (many more constraints compared to
previous model), and modern solvers tend to find it difficult to converge to an optimal solution using
branch-and-bound. 

\subsection{Why Use Flow Model}
LP-relaxation of the flow model can provide valuable information that can help the route generation in the
previous model. (The LP relaxation does not take long to solve.)

For example, if a certain $x_{ijk}$ is close to unity for a given $i \in F$, $(j,k) \in E$, 
then during our route generation we know that among the choices available at $j$, $k$ should be explored 
before other nodes. In context of reinforcement learning, the solution produced by the flow model can better inform what action should be taken in any given state.


% \part{Random Thoughts}

% TODO: Needs better organization.

% \section{Route Generation}

% The key to producing high quality solutions is to generate as many routes
% as possible, with as wide a coverage of available network and loads. This
% will involve a process akin to a depth-first search on a graph. 
% The current optimizer does something similar in its ``simulation'' phase.
% It will be helpful to learn some of the strategies they have found useful.

% Routes are defined in terms of a driver driving along the network, picking
% up and drop-off loads.
% Moreover, routes are allowed to have overlaps in terms of loads and drivers
% during route generation (it is the jobs of the mathematical formulation to
% make sure a feasible subset is ultimately selected).
% Therefore, we can generate routes for each driver independently in parallel.
% \textit{Good parallelization is key to making this work for a problem of this scale.}

% \subsection{Cost Model}
% It is very important to have an accurate cost model for a given route. It 
% should include things like fuel costs, wear and tear, as well as penalties
% for drivers' quality of life issues such as extension fo home time, 
% reductions in previously assigned revenue miles and frequent disruptions to 
% prior routes.

% While cost model is integral part of route generation heuristics, we must 
% also have a stand alone executable that will evaluate any given set of routes.
% This will help with comparing different solutions and fine-tuning the 
% optimization process.

% \section{Input Output Format}
% We need to preserve input and output files, and be able to run the optimizer independently 
% (disconnected from network databases). 
% SQLite can export to many different formats from there, 
% and also makes analysis of solutions easier.

% \section{Real-Time Rerouting For More Lucrative Freight}
% Real-time availability of more lucrative freight opens the possibility of
% rerouting trucks to reduce deadheads, perhaps even alter existing plan
% to improve profitability. 

% Re-router can be a separate module that looks at the sub-graph in the
% vicinity of a given truck, generates more routes based on newly available 
% freight, and re-optimizes to determine if the truck should be re-routed to
% service these new opportunities. Again, this highlights the importance of
% having a trust-worthy cost model to begin with. 

% Note that re-routing to pickup a more lucrative load, while abandoning a prior
% load that the truck had previously promised to service, ultimately involves
% a cost that somebody, somewhere has to pay. Therefore it should also figure
% in the cost model.

% \section{Pseudocode}
% \begin{lstlisting}
% 	// Comment
% 	int main(int argc, char** argv) {
% 		std::cout << "Hello World\n";
% 		return 0;
% 	}
	
% 	enum location_type {
% 		pickup,
% 		dropoff,
% 		transfer	
% 	};
	
% 	struct location {
% 		double lat;
% 		double long;
% 		location_type loc_type;
% 	};
	
% 	std::vector<location> locs;
	
% 	struct leg {
% 		location origin;
% 		location destination;
% 		std::chrono::system_clock::time_point departs;	std::chrono::system_clock::time_point arrives;
% 	}
		
% 	struct route {
% 		std::vector<leg> legs;
% 		int driver_index;
% 		int tractor_index;
% 		std::vector<int> trailer_inds;
% 	}
	
% \end{lstlisting}

\end{document}
