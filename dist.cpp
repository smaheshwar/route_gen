#include "dist.h"

namespace dist
{


double 
haversine(
    double lat1_deg, 
    double lon1_deg, 
    double lat2_deg, 
    double lon2_deg,
    double earth_radius
    )
{
    const double lat_diff_deg = (lat1_deg - lat2_deg);
    const double lon_diff_deg = (lon1_deg - lon2_deg);
    const double lat_diff_rad = deg_to_rad(lat_diff_deg);
    const double lon_diff_rad = deg_to_rad(lon_diff_deg);
    const double lat1_rad = deg_to_rad(lat1_deg);
    const double lat2_rad = deg_to_rad(lat2_deg);
    const double tmp1 = sin(lat_diff_rad/2) * sin(lat_diff_rad/2);
    const double tmp2 = cos(lat1_rad);
    const double tmp3 = cos(lat2_rad);
    const double tmp4 = sin(lon_diff_rad/2) * sin(lon_diff_rad/2);
    const double tmp5 = sqrt(tmp1 + (tmp2*tmp3*tmp4));
    const double result = 2 * earth_radius * asin(tmp5);
    return result;
}

double 
haversine_km(
    double lat1_deg, 
    double lon1_deg, 
    double lat2_deg, 
    double lon2_deg
    )
{
    const double result = 
        haversine(lat1_deg, lat2_deg, lon1_deg, lon2_deg, earth_radius_km);
    return result;
}

double 
haversine_miles(
    double lat1_deg, 
    double lon1_deg, 
    double lat2_deg, 
    double lon2_deg
    )
{
    const double result = 
        haversine(lat1_deg, lat2_deg, lon1_deg, lon2_deg, earth_radius_miles);
    return result;
}

double
get_dist(
    double lat1_deg, 
    double lon1_deg, 
    double lat2_deg, 
    double lon2_deg
    )
{
    const double dist_miles = 
        haversine_miles(lat1_deg, lon1_deg, lat2_deg, lon2_deg);
    return dist_miles;
}


} // end namespace dist