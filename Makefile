# Compiler
# CC = g++
CC = clang++

# Compiler flags
CFLAGS = -g -Wall -std=c++14

all: routegen

routegen: main.o route.o time.o tz.o dist.o
	$(CC) $(CFLAGS) -lcurl -lpthread main.o route.o time.o tz.o dist.o -o routegen 

main.o: main.cpp time.h
	$(CC) $(CFLAGS) -c main.cpp

route.o: route.cpp route.h time.h
	$(CC) $(CFLAGS) -c route.cpp

time.o: time.h time.cpp
	$(CC) $(CFLAGS) -c time.cpp

tz.o: date/tz.cpp date/tz.h date/date.h
	$(CC) $(CGLAGS) -c date/tz.cpp

dist.o: dist.cpp dist.h
	$(CC) $(CGLAGS) -c dist.cpp

clean:
	$(RM) routegen *.o
