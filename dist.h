#include<cmath>

namespace dist
{

const double earth_radius_km = 6372.8;
const double earth_radius_miles = 3958.8;

// Convert given degrees to radians
double deg_to_rad(double degrees) {
    return (degrees * M_PI / 180);
}

double km_to_miles(double km) {
    return (km * 0.62137);
}

// Computes distance based on Haversine formula.
double 
haversine(
    double lat1_deg, 
    double lon1_deg, 
    double lat2_deg, 
    double lon2_deg,
    double earth_radius
    );


double 
haversine_km(
    double lat1_deg, 
    double lon1_deg, 
    double lat2_deg, 
    double lon2_deg
    );

double 
haversine_miles(
    double lat1_deg, 
    double lon1_deg, 
    double lat2_deg, 
    double lon2_deg
    );


// Calculate everytime for now
// Long-term goal: look up a distance matrix that uses cached HERE distances
// as much as possible, and calculates Haversine formulas for other.
double
get_dist(
    double lat1_deg, 
    double lon1_deg, 
    double lat2_deg, 
    double lon2_deg
    );

}