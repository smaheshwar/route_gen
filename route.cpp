#include "route.h"
#include <cassert>

// Prints recaps array in a single line
// [day0, day1, day2, day3, day4, day5, day6]
// day0 is the recap 8 days ago.
// day6 is the recap 1 day agao.
std::ostream&
operator<<(
	std::ostream &os,
	std::array<duration, 7> recaps
	)
{
	os << "recaps: [";
	for(auto r : recaps) {
		os << r << " ";
	}
	os << "]";
	return os;
}

// Prints hos clocks in a single line
// [8_hr_clock, 11_hr_clock, 14_hr_clock, 70_hr_clock]
std::ostream&
operator<<(
	std::ostream &os,
	const hos_clocks &hos
	) 
{
	os << "hos: [";
	os << hos.hos_8 << ",";
	os << hos.hos_11 << ",";
	os << hos.hos_14 << ",";
	os << hos.hos_70 ;
	os << "]";
    return os;
}

// Prints node info
std::ostream&
operator<<(
	std::ostream &os,
	const node &n
	) 
{
	os << "Node:\n";
	os << "\t" << n.timepoint << "\n";
	os << "\t" << n.hos << "\n";
	os << "\t" << n.hos.recaps << "\n";
	os << "\t" << "duty since midnight: " << n.hos.duty_since_last_midnight;
    return os;
}


// Prints segment type
std::ostream&
operator<<(
	std::ostream &os,
	const segment_type st
	) 
{
	switch (st)
	{
	case segment_type::dhd_travel:
		os << "dhd_travel";
		break;
	case segment_type::rev_travel:
		os << "rev_travel";
		break;
	case segment_type::load:
		os << "load";
		break;
	case segment_type::unload:
		os << "unload";
		break;
	case segment_type::rest_30m:
		os << "rest_30m";
		break;
	case segment_type::rest_10h:
		os << "rest_10h";
		break;
	case segment_type::rest_34h:
		os << "rest_34h";
		break;
	default:
		assert(false);
		break;
	}
	return os;
}

// Prints a vector of segments as a series of nodes and 
// corresponding segment types
std::ostream&
operator<<(
	std::ostream &os,
	const vec1d<segment> &segs
	) 
{
	os << "Segments:\n";
	for(const auto s : segs) {
		os << "Next segment: type = " << s.type << std::endl;
		os << "begin node\n";
		os << s.begin << std::endl;
		os << "end node\n";
		os << s.end << std::endl;
	}
	// os << segs[0].begin << std::endl;
	// os << segs[0].type << " starts." << std::endl;
	// for(size_t i = 1; i < segs.size(); ++i) {
	// 	os << segs[i].begin << std::endl;
	// 	os << segs[i].type << " starts." << std::endl;
	// }
	// os << segs.back().end << std::endl;
	// os << segs.back().type << " ends." << std::endl;
	return os;
}


// Helper function.
// Rotates the recaps array.
// Sets the last elment of the rotated array equal to accumulated_duty.
// Performs the following operation.
// recaps = [a,b,c,d,e,f,g] before rotation
// After rotation recaps = [b,c,d,e,f,g,a]
// After updating last element recaps = [b,c,d,e,f,g,accumulated_duty]
void
update_recaps(
	std::array<duration, 7> &recaps,
	const duration &accumulated_duty
	)
{
	auto first = recaps.begin();
	auto second = recaps.begin() + 1;
	auto last = recaps.end();
	std::rotate(first, second, last);
	recaps.back() = accumulated_duty;
}


// Helper function for depleting clocks other than 70hr clock
// If depl_14hr_only is false then 8hr, 11hr and 14hr clocks are depleted      
// If depl_14hr_only is true, then only 14hr clock is depleted
// (70hr clock is updated at midnight only)
// Clock(s) are depleted by amount equal to (t2-t1)
// Clocks are not allowed to become negative
void
deplete_clocks(
	hos_clocks& hos,
	const time_point& t1,
	const time_point& t2,
	bool depl_14hr_only
	)
{
	using std::chrono::duration_cast;
	duration zero_dur(0);
	duration deplt_amt = 
		duration_cast<std::chrono::minutes>(t2 - t1);
	hos.hos_14 = std::max(zero_dur, (hos.hos_14 - deplt_amt));
	if (!depl_14hr_only) {
		hos.hos_8 = std::max(zero_dur, (hos.hos_8 - deplt_amt));
		hos.hos_11 = std::max(zero_dur, (hos.hos_11 - deplt_amt));
	}
}


// Helper function for getting the node when stationary and off duty.
// Previous node is prev, next node (return value) is at cur_time
// if midnight then recaps are rotated
// cumulative duty since last midnight set to 0
// Only 14hr clock depletes
// Caller is responsible for making sure that midnight=true if and only if
// cur_time represents midnight.
node
get_next_node_off_duty(
	const node& prev,
	const time_point& cur_time,
	bool midnight
	)
{
	assert(prev.timepoint < cur_time);
	assert(midnight == is_midnight(cur_time));
	node result(cur_time, prev.hos);
	deplete_clocks(result.hos, prev.timepoint, cur_time, true);
	if (midnight) {
		result.hos.duty_since_last_midnight = duration(0);
		update_recaps(result.hos.recaps, prev.hos.duty_since_last_midnight);
	}
	return result;
}





// Helper function for getting the node when stationary and on duty.
// Previous node is prev, next node (return value) is at cur_time
// All clocks deplete
// if midnight then recaps are rotated (and 70hr clock updated)
// Caller is responsible for making sure that midnight=true if and only if
// cur_time represents midnight.
node
get_next_node_on_duty(
	const node& prev,
	const time_point& cur_time,
	bool midnight
	) 
{
	assert(midnight && is_midnight(cur_time));
	using std::chrono::duration_cast;
	using std::chrono::minutes;
	node result(cur_time, prev.hos);
	deplete_clocks(result.hos, prev.timepoint, cur_time, false);
	if (midnight) {
		duration duty_since_prev = 
			duration_cast<minutes>(cur_time - prev.timepoint);
		duration total_duty_since_last_midnight = 
			prev.hos.duty_since_last_midnight + duty_since_prev;
		result.hos.hos_70 = result.hos.hos_70 + 
				result.hos.recaps.front() - 
				total_duty_since_last_midnight;
		result.hos.hos_70 = std::max(duration(0), result.hos.hos_70);
		update_recaps(result.hos.recaps, total_duty_since_last_midnight);
		result.hos.duty_since_last_midnight = duration(0);
	}
	return result;
}



// Resets clocks given sufficient off-duty time.
// 30min off duty => 8hr clock resets
// 10hr off duty => 8hr, 11hr, 14hr clocks reset
// 34hr off duty => all clocks reset
void 
reset_clocks(
	hos_clocks& hos, 
	const duration& total_off_duty_dur
	)
{
	if (total_off_duty_dur >= duration(34 * 60)) {
		hos.hos_70 = duration(70 * 60);
		hos.hos_11 = duration(11 * 60);
		hos.hos_14 = duration(14 * 60);
		hos.hos_8 = duration(8 * 60);
	}
	else if (total_off_duty_dur >= duration(10 * 60)) {
		hos.hos_11 = duration(11 * 60);
		hos.hos_14 = duration(14 * 60);
		hos.hos_8 = duration(8 * 60);
	}
	else if (total_off_duty_dur >= duration(30)) {
		hos.hos_8 = duration(8 * 60);
	}
}


// For simulating when a vehicle is stationary and off-duty (rest or dwell)
// Returns a vector of nodes. 
// Caller should make segments with appropriate segment_type.
// Impossible to have more than 1 midnight crossing in 30min or 10hr rest.
// Impossible to have more than 2 midnight crossing2 in 34hr rest.
// Rest or dwell lasting 30mins or more resets 8hr clock.
// Rest or dwell lasting 10hr or more resets 11hr and 14hr clocks.
// Rest or dwell lasting 34hr or more resets all clocks.
// Even though 14hr clock depletes during 30min rest, 
// no duty is accumulated for recap purposes.
vec1d<node>
simulate_stationary_off_duty(
	const node& start_node,
	const time_point& end_tm
	)
{
	using std::chrono::duration_cast;
	using std::chrono::minutes;
	vec1d<node> result;
	result.push_back(start_node);
	const time_point& start_tm = start_node.timepoint;
	time_point t = start_tm;
	while (t < end_tm) {
		time_point midnt = get_next_midnight(t);
		if (midnt <= end_tm) {
			node n = 
				get_next_node_off_duty(result.back(), midnt, true);
			result.push_back(n);
			t = midnt;
		}
		else {
			node final_node = 
				get_next_node_off_duty(result.back(), end_tm, false);
			auto tmp = end_tm - start_tm;
			const duration total_off_duty_dur = 
				duration_cast<minutes>(tmp);
			reset_clocks(final_node.hos, total_off_duty_dur);
			result.push_back(final_node);
			break;
		}
	}
	return result;
}


// For simulating when vehicle is stationary an on duty (loading, unloading).
// Returns a vector of nodes.
// Caller should make segments with appropriate segment_type.
// Duty accumulated simply by passing time.
// All clocks deplete, 70hr clock updated at midnight (if midnight crossings
// encountered).
// Clocks are not allowed to go negative if they run out, 
// but no rest is trigered (i.e., loading or unloading continues). 
// No clocks get reset.
vec1d<node>
simulate_stationary_on_duty(
	const node& start_node,
	const time_point& end_tm
	)
{
	using std::chrono::duration_cast;
	vec1d<node> result;
	result.push_back(start_node);
	const time_point& start_tm = start_node.timepoint;
	time_point t = start_tm;
	while (t < end_tm) {
		time_point midnt = get_next_midnight(t);
		if (midnt <= end_tm) {
			node n = 
			get_next_node_on_duty(result.back(), midnt, true);
			result.push_back(n);
			t = midnt;
		}
		else {
			node final_node = 
			get_next_node_on_duty(result.back(), midnt, false);
			result.push_back(final_node);
			break;
		}
	}
	return result;
}



// Returns the estimated time of arrival.
// Computes the current position based on the given cur_time.
// From the current position, the distance left to cover is calculated.
// The distance left to cover is divided by the speed.
// Assume remaining_dstn_dist is in miles, speed is in miles per hour.
time_point
get_dstn_arrival(
	const time_point& cur_time,
	double remaining_dstn_dist,
	double speed 
	)
{
	using std::chrono::duration_cast;
	using std::chrono::minutes;
	duration remaining_dur((int)(remaining_dstn_dist * 60 / speed));
	time_point result = cur_time + remaining_dur;
	return result;
}



// Helper function for simulate_driving().
// Calculates the next node if a midnight crossing is encountered 
// during driving.
// Previous node is prev_node, the next node (return value) is constructed
// for cur_time (assumed to be midnight).
// Therefore recaps are updated, 70hr clock is updated.
node
get_next_node_driving_midnight(
	const node& prev_node,
	const time_point& cur_time // assume midnight
	)
{
	using std::chrono::duration_cast;
	using std::chrono::minutes;
	node result = prev_node;
	result.timepoint = cur_time;
	deplete_clocks(result.hos, prev_node.timepoint, result.timepoint, 
			false);
	result.hos.duty_since_last_midnight = duration(0);
	const duration duty_since_prev_node = 
		duration_cast<minutes>(cur_time - prev_node.timepoint);
	const duration total_duty_since_prev_midnight = 
				duty_since_prev_node + prev_node.hos.duty_since_last_midnight;
	result.hos.hos_70 = result.hos.hos_70 + result.hos.recaps.front() - 
						total_duty_since_prev_midnight;
	result.hos.hos_70 = std::max(duration(0), result.hos.hos_70);
	update_recaps(result.hos.recaps, total_duty_since_prev_midnight);
	return result;
}



// Helper function for simulate_driving().
// Calculates the next node if the vehicle reached the destination.
// Previous node is prev_node, next_node (return value) is made at cur_time.
// All clocks (except 70 hr clock) are depleted. 
// Assume no midnight crossings occur during this time.
node get_next_node_driving_arrive_dstn(
	const node& prev_node,
	const time_point& cur_time 
	)
{
	using std::chrono::duration_cast;
	using std::chrono::minutes;
	node result = prev_node;
	result.timepoint = cur_time;
	deplete_clocks(result.hos, prev_node.timepoint, result.timepoint, 
			false);
	auto new_duty = duration_cast<minutes>(cur_time - prev_node.timepoint);
	result.hos.duty_since_last_midnight = 
		prev_node.hos.duty_since_last_midnight + new_duty;
	return result;
}


// Calculates the next segment given the start node and the amount of rest.
// May return more than one segments because midnight crossings may occur
// during rest.
// Rest is assumed to start at rest_start_node.
// The amount of rest (rest_dur) is provided by caller.
// segment_type (st) is provided by caller since it can vary based on rest_dur.
// Calls simulate_stationary_off_duty() to calculate the nodes.
vec1d<segment>
get_rest_segments(
	const node& rest_start_node,
	const duration &rest_dur,
	const segment_type st 
	)
{
	vec1d<segment> result;
	const time_point rest_end_tm = rest_start_node.timepoint + rest_dur;
	vec1d<node> nodes = simulate_stationary_off_duty(rest_start_node, 
							rest_end_tm);
	for (unsigned int i = 0; i < nodes.size()-1; ++i) {
		segment s(nodes[i], nodes[i+1], st);
		// s.begin = nodes[i];
		// s.end = nodes[i+1];
		// s.type = st;
		result.push_back(s);
	}
	return result;
}


// Helper function for simulate_driving().
// Returns an array containing times of all the possible epochs that could
// happen immediately after the current time (t).
std::array<time_point, 6>
get_driving_epochs(
	const time_point &t,
	const hos_clocks &hos,
	const time_point &dstn_arrival
	)
{
	std::array<time_point, 6> epochs;
	const time_point& midnt = get_next_midnight(t);
	epochs[driving_epochs::midnight] = midnt;
	epochs[driving_epochs::dstn_arrival] = dstn_arrival;
	const time_point& clock_8hr_clk_runs_out = t + hos.hos_8;
	epochs[driving_epochs::clk_8hr_runs_out] = clock_8hr_clk_runs_out;
	const time_point& clock_11hr_clk_runs_out = t + hos.hos_11;
	epochs[driving_epochs::clk_11hr_runs_out] = clock_11hr_clk_runs_out;
	const time_point& clock_14hr_clk_runs_out = t + hos.hos_14;
	epochs[driving_epochs::clk_14hr_runs_out] = clock_14hr_clk_runs_out;
	const time_point& clock_70hr_clk_runs_out = t + hos.hos_70;
	epochs[driving_epochs::clk_70hr_runs_out] = clock_70hr_clk_runs_out;
	return epochs;
}



// Helper function for simulate_driving().
// Calculates the next segment if a midnight crossing is encountered.
// Calls get_next_node_driving_midnight() to calculate the next node.
segment
get_next_segment_driving_midnight(
	const node& prev_node,
	const time_point& t,
	const segment_type st
	)
{
	node next_node = get_next_node_driving_midnight(prev_node, t);
	segment s(prev_node, next_node, st);
	// s.begin = prev_node;
	// s.end = next_node;
	// s.type = st;
	return s;
}


// Helper function for simulate_driving().
// Calculates the next segment if the vehicle reached the destination.
// Previous node is prev_node. The vehicle is assumed to reach destination at t.
// All clocks (except 70 hr clock) are depleted (assumes no midnight crossing).
segment
get_next_segment_driving_dstn_arrival(
	const node& prev_node,
	const time_point& t,
	const segment_type st
)
{
	using std::chrono::duration_cast;
	using std::chrono::minutes;
	node next_node = prev_node;
	next_node.timepoint = t;
	deplete_clocks(next_node.hos, prev_node.timepoint, next_node.timepoint, 
					false);
	const auto new_duty = duration_cast<minutes>(t - prev_node.timepoint);
	next_node.hos.duty_since_last_midnight = 
		prev_node.hos.duty_since_last_midnight + new_duty;
	segment s(prev_node, next_node, st);
	// s.begin = prev_node;
	// s.end = next_node;
	// s.type = st;
	return s;
}


// Helper function for simulate_driving().
// Calculates segments if one of the clocks run out and rest is required.
// Previous node is prev_node. Some clock runs out (rest begins) at time t.
// Assume no midnight crossings between prev_node and t.
// Rest duration (rest_dur) is provided by caller with appropriate 
// segment_type (st).
// Returns vector of segments.
vec1d<segment>
get_rest_segments_driving(
	const node& prev_node,
	const time_point& t,
	segment_type st,
	const duration &rest_dur
	)
{
	using std::chrono::duration_cast;
	using std::chrono::minutes;
	vec1d<segment> result;
	node next_node = prev_node;
	next_node.timepoint = t;
	deplete_clocks(next_node.hos, prev_node.timepoint, 
					next_node.timepoint, false);
	const auto new_duty = duration_cast<minutes>(t - prev_node.timepoint);
	next_node.hos.duty_since_last_midnight = 
		prev_node.hos.duty_since_last_midnight + new_duty;
	segment s(prev_node, next_node, st);
	// s.begin = prev_node;
	// s.end = next_node;
	// s.type = st;
	result.push_back(s);
	vec1d<segment> rest_segs;
	if (rest_dur == duration(30)) {
		rest_segs = get_rest_segments(next_node, rest_dur, 
						segment_type::rest_30m);
	}
	else if (rest_dur == duration(10*60)) {
		rest_segs = get_rest_segments(next_node, rest_dur, 
						segment_type::rest_10h);
	}
	else if (rest_dur == duration(34 * 60)) {
		rest_segs = get_rest_segments(next_node, rest_dur, 
						segment_type::rest_34h);
	}
	else {
		assert(false);
	}
	assert(rest_segs.size() >= 1);
	result.insert(result.cend(), rest_segs.cbegin(), rest_segs.cend());
	return result;
}


// Helper function called by simulate_driving()
// Once the next epoch has been determined (midnight crossing, reaching the
// destination, some clock running out etc.), this function creates the 
// appropriate segments and returns them as a vector.
//
// Following cases are considered based on the parameter indx.
// if (indx == driving_epochs::midnight)
// midnight crossing encountered first
// haven't arrived at destination yet
// no clocks have run out
// update recaps and 70hr clock
// deplete other clocks also
// create segment (prev_node, new_node), 
// vehicle is driving during this segment, 
// dhd or rev according to flag in parameters
//
// if (indx == driving_epochs::dstn_arrival)
// arrived at destination
// no midnight crossings encountered since t
// no clocks ran out
// deplete 8h, 11h, 14h clocks
// create final node
// create segment (prev_node, final_node), 
// vehicle is driving during this segment (st is dhd or rev)
// add to result
// break out of while loop inside simulate_driving() function.
// 
// (indx == driving_epochs::clk_8hr_runs_out)
// 8hr clock runs out before arriving at destination
// create segments for 30min rest
// add new segments to result
// 
// if (indx == driving_epochs::clk_11hr_runs_out || 
// 		indx == driving_epochs::clk_14hr_runs_out)
// 11hr or 14hr clock runs out before arriving at destination
// create segments for 10hr rest
// add new segments to result
//
// if (indx == driving_epochs::clk_70hr_runs_out)
// 70hr clock runs out before arriving at destination
// create segments for 34hr rest
// add new segments to result
vec1d<segment>
process_next_epoch_driving(
	int indx,
	const time_point& t,
	const node &prev_node,
	segment_type st
	)
{
	vec1d<segment> result;
	if (indx == driving_epochs::midnight) {
		segment s = get_next_segment_driving_midnight(prev_node, t, st);
		result.push_back(s);
	}
	else if (indx == driving_epochs::dstn_arrival) {
		segment s = get_next_segment_driving_dstn_arrival(prev_node, t, st);
		result.push_back(s);
	}
	else if (indx == driving_epochs::clk_8hr_runs_out) {
		vec1d<segment> rest_segs = 
			get_rest_segments_driving(prev_node, t, st, duration(30));
		result.insert(result.cend(), rest_segs.cbegin(), rest_segs.cend());
	}
	else if (indx == driving_epochs::clk_11hr_runs_out || 
			indx == driving_epochs::clk_14hr_runs_out) {
		vec1d<segment> rest_segs = 
			get_rest_segments_driving(prev_node, t, st, duration(10*60));
		result.insert(result.cend(), rest_segs.cbegin(), rest_segs.cend());
	}
	else if (indx == driving_epochs::clk_70hr_runs_out) {
		vec1d<segment> rest_segs = 
			get_rest_segments_driving(prev_node, t, st, duration(34 * 60));
		result.insert(result.cend(), rest_segs.cbegin(), rest_segs.cend());
	}
	else assert(false); // should never come here!
	return result;
}


double
get_dist_covered(
	const time_point &t1,
	const time_point &t2,
	double speed
)
{
	using std::chrono::duration_cast;
	using std::chrono::seconds;
	const auto elapsed_sec = duration_cast<seconds>(t2 - t1);
	const double dist_covered = elapsed_sec.count()*speed/3600;
	return dist_covered;
}


// Main function to use for driving simulation.
// Driving is assumed to begin at start_node.
// Distance to destination is dstn_dist (assume miles).
// Average speed is given as speed (assume miles per hour).
// Segment type (st) is given (should be either dhd or rev travel).
vec1d<segment>
simulate_driving(
	const node& start_node,
	double dstn_dist, 
	double speed, 
	segment_type st 
)
{
	vec1d<segment> result;
	const time_point& start_tm = start_node.timepoint;
	time_point t = start_tm;
	time_point dstn_arrival_time = 
		get_dstn_arrival(t, dstn_dist, speed);
	node prev_node = start_node;
	double dist_covered = 0;
	while (dist_covered < dstn_dist)
	{
		std::array<time_point, 6> epochs = 
			get_driving_epochs(t, prev_node.hos, dstn_arrival_time);
		auto it = std::min_element(epochs.cbegin(), epochs.cend());
		t = *it;
		int indx = std::distance(epochs.cbegin(), it);
		vec1d<segment> segs = 
			process_next_epoch_driving(indx, t, prev_node, st);
		const double dist_driven = get_total_dist_covered(segs, speed);
		dist_covered += dist_driven;
		assert(dist_covered <= dstn_dist);
		result.insert(result.end(), segs.begin(), segs.end());
		if (indx == driving_epochs::dstn_arrival) break;
		const auto next_depart_time = segs.back().end.timepoint;
		dstn_arrival_time = 
			get_dstn_arrival(next_depart_time, dstn_dist-dist_covered, speed);
		prev_node = result.back().end;
	}
	return result;
}

double
get_total_dist_covered(
	const vec1d<segment> segs,
	double speed
)
{
	double dist_covered = 0;
	for(const auto s : segs) {
		if(s.type == segment_type::dhd_travel || 
			s.type == segment_type::rev_travel) {
			const auto t1 = s.begin.timepoint;
			const auto t2 = s.end.timepoint;
			const double dist = get_dist_covered(t1, t2, speed);
			dist_covered += dist;
		}
	}
	return dist_covered;
}

vec1d<segment>
simulate_dwell(
	const node &dwell_start_node,
	const time_point &dwell_end
)
{
	vec1d<segment> result;
	vec1d<node> dwell_nodes = 
		simulate_stationary_off_duty(dwell_start_node, dwell_end);
	for(auto i = 0; i < dwell_nodes.size()-1; ++i) {
		segment s(dwell_nodes[i], dwell_nodes[i+1], segment_type::dwell);
		result.push_back(s);
	}
	return result;
}

vec1d<segment>
simulate_load_unload(
	const node &start_node,
	const time_point &end_tm,
	segment_type st
)
{
	vec1d<segment> result;
	vec1d<node> load_unload_nodes = 
		simulate_stationary_on_duty(start_node, end_tm);
	vec1d<segment> load_unload_segs;
	for(auto i = 0; i < load_unload_nodes.size()-1; ++i) {
		segment s(load_unload_nodes[i], load_unload_nodes[i+1], st);
		result.push_back(s);
	}
	return result;
}

vec1d<segment>
simulate_cust_site_visit(
	const node &start_node,
	double dstn_dist,
	const time_point &earliest_arrival,
	const time_point &latest_arrival,
	bool is_pickup,
	const duration& load_unload, 
	double speed)
{
	vec1d<segment> result;
	segment_type st = is_pickup ? 
		segment_type::dhd_travel : segment_type::rev_travel;
	vec1d<segment> segs = simulate_driving(start_node, dstn_dist, speed, st);
	const time_point arrival_time = segs.back().end.timepoint;
	result.insert(result.end(), segs.begin(), segs.end());
	if(arrival_time < earliest_arrival) {
		node dwell_start_node = result.back().end;
		vec1d<segment> dwell_segs = 
			simulate_dwell(dwell_start_node, earliest_arrival);
		result.insert(result.end(), dwell_segs.begin(), dwell_segs.end());
	}
	if(arrival_time < latest_arrival) {
		// simulate load/unload
		node load_unload_start = result.back().end;
		time_point load_unload_end_tm = 
			result.back().end.timepoint + load_unload;
		segment_type tmp = is_pickup ? 
			segment_type::unload : segment_type::load;
		vec1d<segment> load_unload_segs = 
			simulate_load_unload(load_unload_start, load_unload_end_tm, tmp);
		result.insert(result.end(), 
			load_unload_segs.begin(), load_unload_segs.end());
	}
	else {
		// arrived too late, return empty vector
		result.clear();
	}
	return result;
}

vec1d<segment>
simulate_order_delivery(
	double cur_lat,
	double cur_lon,
	const node &start_node,
	const customer_order &order,
	double dist_to_pickup,
	double speed
)
{
	vec1d<segment> result;
	// simulate pickup
	vec1d<segment> pu_segs = 
		simulate_cust_site_visit(start_node, dist_to_pickup, 
			order.pickup.window_start, order.pickup.window_end, true, 
				order.pickup.load_unload, speed);
	if(pu_segs.size()) {
		result.insert(result.end(), pu_segs.begin(), pu_segs.end());
		for(auto drop : order.dropoff) {
			vec1d<segment> drop_segs =
				simulate_cust_site_visit(result.back().end, drop.dist_from_prev,
					drop.window_start, drop.window_end, false, drop.load_unload,
						speed);
			if(drop_segs.size() == 0) {
				result.clear();
				break;
			}
			result.insert(result.end(), drop_segs.begin(), drop_segs.end());
		}
	}
	return result;
}
