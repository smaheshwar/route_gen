#include "time.h"

bool
is_midnight(const time_point& tp)
{
	using std::chrono::duration_cast;
	using std::chrono::seconds;
	const seconds secs_in_day(24*3600);
	const seconds epoch = duration_cast<seconds>(tp.time_since_epoch());
	const seconds elapsed_since_midnight = epoch % secs_in_day;
	return (elapsed_since_midnight == seconds(0));
	// Alternate possibly less efficient way of checking midnight
	// std::time_t tmp1 = std::chrono::system_clock::to_time_t(tp);
	// std::tm* tmp2 = std::localtime(&tmp1);
	// return (all_equal(0, tmp2->tm_hour, tmp2->tm_min, tmp2->tm_sec));
}


time_point
get_last_midnight(const time_point& tp)
{
	time_point last_midnight = tp;
	using std::chrono::duration_cast;
	std::time_t tmp1 = std::chrono::system_clock::to_time_t(tp);
	std::tm* tmp2 = std::localtime(&tmp1);
	if (all_equal(0, tmp2->tm_hour, tmp2->tm_min, tmp2->tm_sec)) {
		const duration day(24 * 60);
		last_midnight = tp - day;
	}
	else {
		tmp2->tm_hour = 0;
		tmp2->tm_min = 0;
		tmp2->tm_sec = 0;
		std::time_t tmp3 = std::mktime(tmp2);
		last_midnight = std::chrono::system_clock::from_time_t(tmp3);
	}
	return last_midnight;
}


time_point
get_next_midnight(const time_point& tp)
{
	using std::chrono::duration_cast;
	using std::chrono::seconds;
	const seconds secs_in_day(24*3600);
	const seconds epoch = duration_cast<seconds>(tp.time_since_epoch());
	const seconds elapsed_since_midnight = epoch % secs_in_day;
	const auto remaining_till_midnight = secs_in_day - elapsed_since_midnight;
	const time_point result = tp + remaining_till_midnight;
	return result;
}

duration
mins_since_midnight(const time_point &tp) 
{
	using std::chrono::duration_cast;
	using std::chrono::minutes;
	time_point last_midnight = get_last_midnight(tp);
	auto tmp = (tp - last_midnight);
	duration result = duration_cast<minutes>(tmp);
	return result;
}


duration
mins_till_midnight(const time_point &tp) 
{
	const duration DAY(24*60);
	auto mins_since = mins_since_midnight(tp);
	auto result = (DAY - mins_since);
	return result;
}


time_point
parse_time_point(const std::string &s) 
{
	using std::chrono::system_clock;
	using std::mktime;
	std::tm t = {};
	// strptime is POSIX only, does not work on Windows
	char *tmp = strptime(s.c_str(), "%Y-%m-%d %H:%M", &t);
	assert(tmp != NULL);
	time_point result = system_clock::from_time_t(mktime(&t));
	return result;
}


std::ostream&
operator<<(
	std::ostream& os,
    const time_point &tp
	) 
{
        char buf[128];
        std::time_t tt = std::chrono::system_clock::to_time_t(tp);
        strftime(buf, sizeof buf, "%c", std::localtime(&tt));
        os << buf;
        return os;
}

std::ostream&
operator<<(
	std::ostream &os,
	const duration &dur
	) 
{
	int hrs = dur.count() / 60;
	int min = dur.count() % 60;
	os << hrs << "h" << min << "m";
    return os;
}

